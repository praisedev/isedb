#!/bin/bash
export LD_LIBRARY_PATH=$HOME/$REPL_SLUG/apt/usr/lib/x86_64-linux-gnu:$PWD/apt/usr/lib
export LIBRARY_PATH=$HOME/$REPL_SLUG/apt/usr/lib/x86_64-linux-gnu:$PWD/apt/usr/lib:
export PATH=$PATH:$HOME/$REPL_SLUG/mysql/bin

cd mysql
bin/mysqld --defaults-file=$HOME/$REPL_SLUG/mysql/my.cnf --initialize-insecure