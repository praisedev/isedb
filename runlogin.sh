#!/bin/bash
export LD_LIBRARY_PATH=$HOME/$REPL_SLUG/apt/usr/lib/x86_64-linux-gnu:$PWD/apt/usr/lib
export LIBRARY_PATH=$HOME/$REPL_SLUG/apt/usr/lib/x86_64-linux-gnu:$PWD/apt/usr/lib:
export PATH=$PATH:$HOME/$REPL_SLUG/mysql/bin

cd mysql
cp my.cnf /tmp/
bin/mysqld --defaults-file=/tmp/my.cnf &
sleep 4s
bin/mysql --defaults-file=/tmp/my.cnf -u root -p